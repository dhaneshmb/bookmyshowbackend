const express = require("express");
//const bodyParser = require("body-parser");
const cors = require("cors");
const signUpRoute = require("./app/routes/signUpRoute");
const loginRoute = require("./app/routes/loginRoute");
const cityRoute = require("./app/routes/cityRoute");
const showRoute = require("./app/routes/showRoute");
const show_cityRoute = require("./app/routes/show_cityRoute")
const show_theater_cityRoute = require("./app/routes/show_theater_cityRoute")
const theaterTablesRoute = require("./app/routes/theaterTablesRoute");
const bookingRoute = require("./app/routes/bookingRoute")

const usertableRoute = require("./app/routes/testusertableRoute.js")



const app = express();

const db = require("./app/models/index");
db.sequelize.sync();

// following code will delete all the tables from the database
// db.sequelize.sync({ force: true }).then(() => {
//     console.log("Drop and re-sync db.");
// });


var corsOptions = {
  origin: "*"

  // origin: "http://localhost:3000"||"https://bookmyshow-clone-clxqkc3ci-chaithanyakumark.vercel.app/"
};

app.use(cors(corsOptions));
app.use(express.urlencoded({ extended: false }));
app.use(express.json());


//routes       
app.use('/signUp', signUpRoute)
app.use('/login', loginRoute)
app.use('/city', cityRoute)
app.use('/show', showRoute)
app.use('/allCityAllShow', show_cityRoute)
app.use('/allShowAllTheater', show_theater_cityRoute)
app.use('/theaterTable', theaterTablesRoute)
app.use('/booking', bookingRoute)
// app.use('/cityName/showType', show_cityRoute)


//test
app.use('/userTable', usertableRoute)


// simple route
app.get("/", (req, res) => {
  res.json({ message: "Welcome to bookMyShow." });
});


// set port, listen for requests
const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
  console.log(`Server is running at http://localhost:${PORT}.`);
});