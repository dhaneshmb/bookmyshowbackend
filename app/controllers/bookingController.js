const { booking } = require('../models/index');

// for inserting user booking details into database.
const bookingFn = (req, res) => {
    const bookingDetails = {
        userName: req.body.userName,
        email: req.body.email,
        showName: req.body.showName,
        seat: req.body.seat,
        noOfSeat: req.body.noOfSeat,
        price: req.body.price,
        theaterName: req.body.theaterName,
        cityName: req.body.cityName,
        date: req.body.date,
        time: req.body.time
    }

    booking.create(bookingDetails)
        .then((bookingData) => {
            res.status(201).json({
                "success": true,
                "data": bookingData
                })
                .end()
        })
        .catch((err) => {
            res.status(500).json({
                    "success": false,
                    "message": err.message || 'Some error occurred while adding booking details.',
                })
                .end()
        })
}

// for fetching user booking details

const findBookingDetailsFn = (req, res) => {
    const emailToFind = req.query.email;
    booking.findAll()
        .then(bookingDataArr => {
            let bookingData = bookingDataArr.filter(obj => obj.email === emailToFind);
            
            if(bookingData.length === 0) {
                bookingData = "Book your first Show"
            }
            res.status(200).json({
                "success": true,
                "data": bookingData
            })
                .end()
        })
        .catch(err => {
            res.status(500).json({
                "success": false,
                "message": err.message || 'Some error occurred while fetching the booking details.',
            })
                .end()
        })

}


bookingFn
module.exports = {
   findBookingDetailsFn: findBookingDetailsFn,
   bookingFn: bookingFn
};