const { city } = require('../models/index');

// for filling the table on post request
const cityFnToFillTable = (req, res) => {
    const cityDetails = {
        name: req.body.name,
        imgLink: req.body.imgLink
    }
    city.create(cityDetails)
        .then((cityData) => {
            res.status(201).json({
                "success": true,
                "data": cityData
            })
                .end()
        })
        .catch((err) => {
            res.status(500).json({
                "success": false,
                "message": err.message || 'Some error occurred while creating the city.',
            })
                .end()
        })
}

// for fetching all the cities from the database on get request
const cityFn = (req, res) => {
    city.findAll()
        .then(cityData => {
            res.status(200).json({
                "success": true,
                "data": cityData
            })
                .end()
        })
        .catch(err => {
            res.status(500).json({
                "success": false,
                "message": err.message || 'Some error occurred while fetching the cities.',
            })
                .end()
        })
}

// for fetching 

module.exports = {
    cityFnToFillTable: cityFnToFillTable,
    cityFn: cityFn
};
