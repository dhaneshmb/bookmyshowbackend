const { pvrAcropolis } = require('../models/index');
const { legend } = require('../models/index');
const { cinepolis } = require('../models/index');
const { time } = require('../models/index');
const { inox } = require('../models/index');


// for fetching all the theater data from the database on get request
const theaterDetailsFn = (req, res) => {
    const theaterName = req.query.theaterName;
    let theater;

    if (theaterName === "PVR") {
        theater = pvrAcropolis;
    }
    if (theaterName === "Leg") {
        theater = legend;
    }
    if (theaterName === "Cin") {
        theater = cinepolis;
    }
    if (theaterName === "Tim") {
        theater = time;
    }
    if (theaterName === "INO") {
        theater = inox;
    }

    theater.findAll()
        .then(theaterData => {
            res.status(200).json({
                "success": true,
                "data": theaterData
            })
                .end()
        })
        .catch(err => {
            res.status(500).json({
                "success": false,
                "message": err.message || 'Some error occurred while fetching the data.',
            })
                .end()
        })
}

// for updating status all the theater seat in the database on post request

const bookSeatFn = (req, res) => {

    const seatArr = req.body.seatName;
    const theaterName = req.query.theaterName;
    let theater;

    if (theaterName === "PVR") {
        theater = pvrAcropolis;
    }
    if (theaterName === "Leg") {
        theater = legend;
    }
    if (theaterName === "Cin") {
        theater = cinepolis;
    }
    if (theaterName === "Tim") {
        theater = time;
    }
    if (theaterName === "INO") {
        theater = inox;
    }

    seatArr.forEach(seat => {
        theater.update(
            { status: "booked" },
            { where: { seats: seat } }
        )
            .then(rowsUpdated => {
                res.status(200).json({
                    "success": true,
                    "rowsUpdated": rowsUpdated
                }).end()
            })
            .catch(err => {
                res.status(500).json({
                    "success": false,
                    "message": err.message || 'Some error occurred while uploading.',
                })
                    .end()
            })
    })
}


module.exports = {
    //cityFnToFillTable: cityFnToFillTable,
    theaterDetailsFn: theaterDetailsFn,
    bookSeatFn: bookSeatFn
};