const { user } = require('../models/index');

const testusertable = (req, res) => {
    user.findAll()
            .then((userData) => {
                res.status(201).json({
                    "success": true,
                    "data": userData,
                    })
                    .end()
            })
            .catch((err) => {
                //console.log('error', err)

                res.status(500).json({
                        "success": false,
                        "message": err.message || 'Some error occurred while fetching the user.',
                    })
                    .end()
            })
}

module.exports = testusertable