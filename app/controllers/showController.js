const { show } = require('../models/index');

// for filling the table on post request
const showFnToFillTable = (req, res) => {
    const arrOfObj = req.body.data;
    arrOfObj.forEach(obj => {

        const showDetails = {
            name: obj.name,
            category: obj.category,
            description: obj.description,
            imgLink: obj.imgLink,
            type: obj.type,
            price: obj.price,
            trailerLink: obj.trailerLink,
            city: obj.city
        }
        show.create(showDetails)
            .then((showData) => {
                res.status(201).json({
                    "success": true,
                    "data": showData
                })
                    .end()
            })
            .catch((err) => {
                res.status(500).json({
                    "success": false,
                    "message": err.message || 'Some error occurred while creating the show.',
                })
                    .end()
            })
    })

}

// for fetching all the shows from the database on get request
const showFn = (req, res) => {
    show.findAll()
        .then(showData => {
            res.status(200).json({
                "success": true,
                "data": showData
            })
                .end()
        })
        .catch(err => {
            res.status(500).json({
                "success": false,
                "message": err.message || 'Some error occurred while fetching the shows.',
            })
                .end()
        })
}

// for fetching shows from the database on the basis of get request url.
const requiredDataFn = (req, res) => {

    const url = req.url.split("/")
    //console.log(url) = ["","movie","recommended"]
    show.findAll({ where: { category: `${url[1]}` } })
        .then(data => {
            let requiredData = data.filter(obj => obj.type === `${url[2]}`);
            requiredData = requiredData.sort(() => Math.random() - 0.5)
            res.status(200).json({
                "success": true,
                "data": requiredData
            })
                .end()
        })
        .catch(err => {
            res.status(500).json({
                "success": false,
                "message": err.message || 'Some error occurred while fetching the shows.',
            })
                .end()
        })
}

module.exports = {
    showFnToFillTable: showFnToFillTable,
    showFn: showFn,
    requiredDataFn: requiredDataFn
};