const { user } = require('../models/index');
const validator = require('validator');

const signUpUserFn = (req, res) => {
        //check if the name, email, password fields are not empty
        console.log(`giving ${req.body}`);
        if(!req.body.name || !req.body.email || !req.body.password){
          res.status(400).json({
              'success': false,
              "message": "Name, Email & Password cannot be empty"
          })
          .end()
          return
        }

        //check if the email id is valid or not
        if(!validator.isEmail(req.body.email)){
          res.status(400).json({
            'success': false,
            "message": "Invalid email id"
          })
          .end()
          return
        }
         
        // check if the user already exist with the same email id
        user.findOne({ where: {email: req.body.email} })
            .then(userData => {
                
                //if userData is null it will create the account
                if(userData === null){

                    const userDetails = {
                        name: req.body.name,
                        email: req.body.email,
                        password: req.body.password,
                    }
            
                    user.create(userDetails)
                        .then((userData) => {
                            res.status(201).json({
                                "success": true,
                                "data": userData
                                })
                                .end()
                        })
                        .catch((err) => {
                            res.status(500).json({
                                    "success": false,
                                    "message": err.message || 'Some error occurred while creating the user.',
                                })
                                .end()
                        })
                // else it will show error
                } else {
                    res.status(500).json({
                        "success": false,
                        "message": "user already exist"
                    })
                    .end()
                }
            })
            .catch((err) => {
                res.status(500).json({
                    "success": false,
                    "message": err.message || 'Some error occurred while confirming the users existence.',
                })
                .end()
            })
        

        // const userDetails = {
        //     name: req.body.name,
        //     email: req.body.email,
        //     password: req.body.password,
        // }

        // user.create(userDetails)
        //     .then((userData) => {
        //         res.status(201).json({
        //             "success": true,
        //             "data": userData
        //             })
        //             .end()
        //     })
        //     .catch((err) => {
        //         //console.log('error', err)

        //         res.status(500).json({
        //                 "success": false,
        //                 "message": err.message || 'Some error occurred while creating the user.',
        //             })
        //             .end()
        //     })
    }


module.exports = signUpUserFn