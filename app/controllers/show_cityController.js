const { show } = require('../models/index');
const { city } = require('../models/index');


//give data of all shows in all cities
const allShowsInAllCityFn = (req, res) => {
    //let url = req.url.split("/");
    city.findAll({
        include: [
          {
            model: show,
            as: "id_show_shows",
            attributes: ["idShow", "name", "category", "description", "imgLink", "type", "price", "trailerLink"],
            through: {
              attributes: [],
            }
          },
        ],
      })
        .then((cities) => {
          res.status(200).json({
            "success": true,
            "data": cities
        })
        .end()
        })
        .catch((err) => {
          console.log(">> Error while retrieving Tags: ", err);
        });

}

const findShows = (cityId) => {
   return city.findByPk(cityId, {
    include: [
      {
        model: show,
        as: "id_show_shows",
        attributes: ["idShow", "name", "category", "description", "imgLink", "type", "price", "trailerLink"],
        through: {
          attributes: [],
        }
      },
    ],
  })
    .then((city) => {
      return city;
    })
    .catch((err) => {
      console.log(">> Error while finding Tag: ", err);
    });
}


//give shows of a particular city(acc to cityId)
const particularShowsOfCityFn = async (req, res) => {
  const cityId = req.query.id;
  const type = req.query.type;
  const category = req.query.category;
  let response;
  // console.log(cityId)
  const data = await findShows(cityId).then((showsInCity) => {

    if(type === "recommended"){
      const recommendedShows = showsInCity.id_show_shows.filter(showObj => showObj.type === "recommended");
      response = recommendedShows;

    }else if(type === "popular"){
      const popularShows = showsInCity.id_show_shows.filter(showObj => showObj.type === "popular");
      response = popularShows;

    }else if(type === "entertainment"){
      const entertainmentShows = showsInCity.id_show_shows.filter(showObj => showObj.type === "entertainment");
      response = entertainmentShows;

    }else{
      response = showsInCity;
    }
    res.status(200).json({
      "success": true,
      "data": response
  })
  .end()
})
  
}

module.exports = {
    allShowsInAllCityFn: allShowsInAllCityFn,
    particularShowsOfCityFn: particularShowsOfCityFn
    // findShowWithCityId:findShowWithCityId
};