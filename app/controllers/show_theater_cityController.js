const { show } = require('../models/index');
const { city } = require('../models/index');
const {theater} = require('../models/index');

const findTheaters = (/*cityId, cityName, showName*/) => {
    return show.findAll( {
     include: [
       {
         model: theater,
         as: "id_theater_theaters",
         attributes: ["idTheater", "idCity", "name"],
         through: {
           attributes: [],
         }
       },
     ],
   })
     .then((show) => {
         //console.log(JSON.stringify(show))
       return show;
     })
     .catch((err) => {
       console.log(">> Error while finding Tag: ", err);
     });
 }

 const requiredShowTheaterDataFn = async (req, res) => {
    const showId = req.query.showId;
    console.log(showId)
    // const cityName = req.query.cityName
    // const showName = req.query.showName
    let response;
    // console.log(cityId)
    const data = await findTheaters().then((allShowsAllTheater) => {
  
      if(showId){
        const requiredShow = allShowsAllTheater.find( showObj => showObj.idShow == showId );
        //console.log(requiredShow)
        response = requiredShow;
  
      }
      //else if(type === "popular"){
    //     const popularShows = shows.id_show_shows.filter(showObj => showObj.type === "popular");
    //     response = popularShows;
  
    //   }else if(type === "entertainment"){
    //     const entertainmentShows = shows.id_show_shows.filter(showObj => showObj.type === "entertainment");
    //     response = entertainmentShows;
  
     else{
        response = allShowsAllTheater;
    }
      res.status(200).json({
        "success": true,
        "data": [response]
    })
    .end()
  })
    
  }
  

//  console.log(findTheaters());
 
module.exports = {
    requiredShowTheaterDataFn: requiredShowTheaterDataFn,
    //particularShowsOfCityFn: particularShowsOfCityFn
    // findShowWithCityId:findShowWithCityId
};