const { user } = require('../models/index');
const validator = require('validator');
// const bcrypt = require('bcrypt')

const loginUserFn = (req, res) => {

    //check if the email, password fields are not empty
    if(!req.body.email || !req.body.password){
      res.status(400).json({
          'success': false,
          "message": "Email & Password cannot be empty"
      })
      .end()
      return
    }
    
    //check if the email id is valid or not
    if(!validator.isEmail(req.body.email)){
      res.status(400).json({
        'success': false,
        "message": "Invalid email id"
      })
      .end()
      return
    }

    // find user with the same email id
    user.findOne({ where: {email: req.body.email} }) 
          .then(userData => {
                  
                  //if user exist check password
                  if(userData.password === req.body.password) {
                    res.status(200).json({
                        "success": true,
                        "data" : userData
                      })
                      .end()
                  //if password is incorrect
                  } else {
                    res.status(500).json({
                        "success": false,
                        "message" : "incorrect password"
                      })
                      .end()
                  }
          })
          .catch(err => {
            res.status(500).json({
                "success": false,
                "message": `User with email Id = ${req.body.email} does not exist.`
            })
            .end()
          });
}

module.exports = loginUserFn
