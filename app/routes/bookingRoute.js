const express = require('express')
const { bookingFn, findBookingDetailsFn } = require('../controllers/bookingController')

const route = express.Router()

// console.log(signUpController)
route.post('/', bookingFn)
route.get('/', findBookingDetailsFn)

module.exports = route
