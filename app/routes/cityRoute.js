const express = require("express");
//const  = require("../controllers/cityController");
const { cityFn, cityFnToFillTable }  = require("../controllers/cityController")
const route = express.Router();

route.post("/", cityFnToFillTable)
route.get("/",cityFn)

module.exports = route