const express = require("express");
//const  = require("../controllers/cityController");
const { allShowsInAllCityFn, particularShowsOfCityFn }  = require("../controllers/show_cityController")
const route = express.Router();

route.get("/",allShowsInAllCityFn)
route.get("/cityId",particularShowsOfCityFn)

//route.get("/showType", particularShowsOfCityFn)

module.exports = route