const express = require("express");
//const  = require("../controllers/cityController");
const { showFn, showFnToFillTable, requiredDataFn }  = require("../controllers/showController")
const route = express.Router();

route.post("/", showFnToFillTable)
route.get("/",showFn) //give data of all the shows
route.get("/movie/recommended",requiredDataFn) //give data of recommended movie
route.get("/movie/popular",requiredDataFn) //give data of popular movie
route.get("/movie/entertainment",requiredDataFn) //give data of entertainment movie

module.exports = route