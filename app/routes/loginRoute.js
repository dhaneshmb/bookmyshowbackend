const express = require('express')
const loginUserFn = require('../controllers/loginController')
const route = express.Router()

route.post('/', loginUserFn)

module.exports = route
