const express = require('express')
const signUpUserFn = require('../controllers/signUpController')

const route = express.Router()

// console.log(signUpController)
route.post('/', signUpUserFn)

module.exports = route
