const express = require("express");
//const  = require("../controllers/cityController");
const { theaterDetailsFn, bookSeatFn }  = require("../controllers/theaterTablesController")
const route = express.Router();

 route.post("/", bookSeatFn)
route.get("/",theaterDetailsFn)

module.exports = route