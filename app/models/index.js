const dbConfig = require("../config/db.config.js");

const Sequelize = require("sequelize");
// const theater = require("./theater.js");
const sequelize = new Sequelize(dbConfig.DB, dbConfig.USERNAME, dbConfig.PASSWORD, {
  host: dbConfig.HOST,
  dialect: "mysql",
});

const db = {};
db.Sequelize = Sequelize
db.sequelize = sequelize

// adding table to db
db.user = require('./user.js')(sequelize, Sequelize)


db.city = require('./city.js')(sequelize, Sequelize)
db.show = require('./show.js')(sequelize, Sequelize)
db.show_city = require('./show_city.js')(sequelize, Sequelize)
db.theater = require('./theater.js')(sequelize, Sequelize)
db.show_theater = require('./show_theater.js')(sequelize, Sequelize)
db.pvrAcropolis = require("./theaterTables/pvrAcropolis")(sequelize, Sequelize)
db.legend = require("./theaterTables/legend")(sequelize, Sequelize)
db.cinepolis = require("./theaterTables/cinepolis")(sequelize, Sequelize)
db.time = require("./theaterTables/time")(sequelize, Sequelize)
db.inox = require("./theaterTables/inox")(sequelize, Sequelize)
db.booking = require("./booking.js")(sequelize, Sequelize)
//adding foreign key
//db.Actor.hasMany(db.Movies)

//one show in many city and one city has many shows(many to many) 
db.show.belongsToMany(db.city, { as: 'id_city_cities', through: db.show_city, foreignKey: "idShow", otherKey: "idCity" });
db.city.belongsToMany(db.show, { as: 'id_show_shows', through: db.show_city, foreignKey: "idCity", otherKey: "idShow" });

//city has many theaters (one to many)
db.theater.belongsTo(db.city, { as: "id_city_city", foreignKey: "idCity"});
db.city.hasMany(db.theater, { as: "theater", foreignKey: "idCity"});

// //one show in many city and one city has many shows(many to many) 
db.show.belongsToMany(db.theater, { as: 'id_theater_theaters', through: db.show_theater, foreignKey: "idShow", otherKey: "idTheater" });
db.theater.belongsToMany(db.show, { as: 'id_show_shows', through: db.show_theater, foreignKey: "idTheater", otherKey: "idShow" });

// // //city has many theaters (one to many)
db.show_theater.belongsTo(db.city, { as: "id_city_city", foreignKey: "idCity"});
db.city.hasMany(db.show_theater, { as: "show_theater", foreignKey: "idCity"});



// console.log(db)
module.exports = db;