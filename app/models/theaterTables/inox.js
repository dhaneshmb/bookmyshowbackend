module.exports = (sequelize, DataTypes) => {
    const theater5 = sequelize.define('inoxTheaterTable', {
      idInoxTheater: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        allowNull: false,
        autoIncrement: true,
      },
      seats: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      status: {
        type: DataTypes.STRING,
        allowNull: false,
      }
    })
    return theater5
  }