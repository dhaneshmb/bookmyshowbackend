module.exports = (sequelize, DataTypes) => {
    const theater3 = sequelize.define('cinepolisTheaterTable', {
      idCinepolisTheater: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        allowNull: false,
        autoIncrement: true,
      },
      seats: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      status: {
        type: DataTypes.STRING,
        allowNull: false,
      }
    })
    return theater3
  }
  