module.exports = (sequelize, DataTypes) => {
    const theater4 = sequelize.define('timeTheaterTable', {
      idTimeTheater: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        allowNull: false,
        autoIncrement: true,
      },
      seats: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      status: {
        type: DataTypes.STRING,
        allowNull: false,
      }
    })
    return theater4
  }