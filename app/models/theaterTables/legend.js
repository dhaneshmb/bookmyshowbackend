module.exports = (sequelize, DataTypes) => {
    const theater2 = sequelize.define('legendTheaterTable', {
      idLegendTheater: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        allowNull: false,
        autoIncrement: true,
      },
      seats: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      status: {
        type: DataTypes.STRING,
        allowNull: false,
      }
    })
    return theater2
  }
  