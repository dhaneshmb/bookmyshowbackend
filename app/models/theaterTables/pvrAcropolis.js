module.exports = (sequelize, DataTypes) => {
    const theater1 = sequelize.define('pvrTheaterTable', {
      idPvrTheater: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        allowNull: false,
        autoIncrement: true,
      },
      seats: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      status: {
        type: DataTypes.STRING,
        allowNull: false,
      }
    })
    return theater1
  }
  