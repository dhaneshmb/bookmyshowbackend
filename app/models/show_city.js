module.exports = (sequelize, DataTypes) => {
    const show_city = sequelize.define('show_cityTable', {
        idShow: {
            type: DataTypes.INTEGER,
            allowNull: false,
            primaryKey: true,
            references: {
              model: 'showTable',
              key: 'idShow'
            }
          },
        idCity: {
            type: DataTypes.INTEGER,
            allowNull: false,
            primaryKey: true,
            references: {
              model: 'cityTable',
              key: 'idCity'
            }
          }
    })
    return show_city
  }
  