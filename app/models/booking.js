module.exports = (sequelize, DataTypes) => {
    const booking = sequelize.define('bookingTable', {
      idBooking: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        allowNull: false,
        autoIncrement: true,
      },
      userName: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      email: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      showName: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      seat: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      noOfSeat: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
      price: {
        type: DataTypes.FLOAT,
        allowNull: false,
      },
      theaterName: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      cityName: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      date: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      time: {
        type: DataTypes.STRING,
        allowNull: false,
      }
    })
    return booking
  }
  