module.exports = (sequelize, DataTypes) => {
    const theater = sequelize.define('theaterTable', {
      idTheater: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        allowNull: false,
        autoIncrement: true,
      },
      idCity: {
        type: DataTypes.INTEGER,
        allowNull: false,
        references: {
          model: 'cityTable',
          key: 'idCity'
        }, 
      },
      name: {
        type: DataTypes.STRING,
        allowNull: false,
      },
  
    })
    return theater
  }
  