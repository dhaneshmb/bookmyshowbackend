module.exports = (sequelize, DataTypes) => {
    const show_theater = sequelize.define('showTheaterTable', {
        idShowTheater: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
            allowNull: false 
        },
        idShow: {
            type: DataTypes.INTEGER,
            allowNull: false,
            primaryKey: true,
            references: {
              model: 'showTable',
              key: 'idShow'
            },
          },
        idTheater: {
            type: DataTypes.INTEGER,
            allowNull: false,
            primaryKey: true,
            references: {
              model: 'theaterTable',
              key: 'idTheater'
            },
          },
        idCity: {
            type: DataTypes.INTEGER,
            allowNull: false,
            references: {
              model: 'cityTable',
              key: 'idCity'
            },
          },
    })
    return show_theater
  }
  