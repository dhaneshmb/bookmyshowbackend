module.exports = (sequelize, DataTypes) => {
    const city = sequelize.define('cityTable', {
      idCity: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        allowNull: false,
        autoIncrement: true,
      },
      name: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      imgLink: {
        type: DataTypes.STRING,
        allowNull: false,
      }
    })
    return city
  }
  