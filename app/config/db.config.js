const dotenv = require('dotenv')
dotenv.config()

module.exports = {
  PORT: process.env.PORT || 3000,
  USERNAME: process.env.DB_USERNAME,
  PASSWORD: process.env.DB_PASSWORD,
  HOST: process.env.DB_HOST,
  DB: process.env.DB_DATABASE,
//   access_token: process.env.ACCESS_TOKEN,
}
